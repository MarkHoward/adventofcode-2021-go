package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Instruction struct {
	position int
	startX   int
	endX     int
	startY   int
	endY     int
	startZ   int
	endZ     int
}

func buildInstruction(line string) Instruction {
	var position int

	parts := strings.Split(line, " ")
	if parts[0] == "on" {
		position = 1
	}

	parts = strings.Split(parts[1], ",")
	x := strings.Split(parts[0], "..")
	y := strings.Split(parts[1], "..")
	z := strings.Split(parts[2], "..")

	startX, _ := strconv.Atoi(x[0][2:])
	endX, _ := strconv.Atoi(x[1])
	startY, _ := strconv.Atoi(y[0][2:])
	endY, _ := strconv.Atoi(y[1])
	startZ, _ := strconv.Atoi(z[0][2:])
	endZ, _ := strconv.Atoi(z[1])

	return Instruction{position, startX, endX, startY, endY, startZ, endZ}
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	var instructions []Instruction
	for _, line := range lines {
		instructions = append(instructions, buildInstruction(line))
	}

	var grid [101][101][101]int
	for _, instruction := range instructions {
		for x := instruction.startX; x <= instruction.endX; x++ {
			if x > 50 || x < -50 {
				continue
			}
			for y := instruction.startY; y <= instruction.endY; y++ {
				if y > 50 || y < -50 {
					continue
				}
				for z := instruction.startZ; z <= instruction.endZ; z++ {
					if z > 50 || z < -50 {
						continue
					}
					grid[x+50][y+50][z+50] = instruction.position
				}
			}
		}
	}

	var count int
	for _, x := range grid {
		for _, y := range x {
			for _, z := range y {
				if z == 1 {
					count++
				}
			}
		}
	}
	fmt.Println(count)
}
