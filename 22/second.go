// Adapted an answer I looked up
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Cube struct {
	position string
	startX   int
	endX     int
	startY   int
	endY     int
	startZ   int
	endZ     int

	value int
}

func (c Cube) GetIntersect(other Cube) *Cube {
	if c.startX > other.endX || c.endX < other.startX {
		return nil
	}
	if c.startY > other.endY || c.endY < other.startY {
		return nil
	}
	if c.startZ > other.endZ || c.endZ < other.startZ {
		return nil
	}

	var startX, endX, startY, endY, startZ, endZ int
	if c.startX > other.startX {
		startX = c.startX
	} else {
		startX = other.startX
	}
	if c.endX < other.endX {
		endX = c.endX
	} else {
		endX = other.endX
	}
	if c.startY > other.startY {
		startY = c.startY
	} else {
		startY = other.startY
	}
	if c.endY < other.endY {
		endY = c.endY
	} else {
		endY = other.endY
	}
	if c.startZ > other.startZ {
		startZ = c.startZ
	} else {
		startZ = other.startZ
	}
	if c.endZ < other.endZ {
		endZ = c.endZ
	} else {
		endZ = other.endZ
	}
	newCube := Cube{other.position, startX, endX, startY, endY, startZ, endZ, (endX - startX + 1) * (endY - startY + 1) * (endZ - startZ + 1)}
	return &newCube
}

func buildCube(line string) Cube {
	parts := strings.Split(line, " ")
	position := parts[0]

	parts = strings.Split(parts[1], ",")
	x := strings.Split(parts[0], "..")
	y := strings.Split(parts[1], "..")
	z := strings.Split(parts[2], "..")

	startX, _ := strconv.Atoi(x[0][2:])
	endX, _ := strconv.Atoi(x[1])
	startY, _ := strconv.Atoi(y[0][2:])
	endY, _ := strconv.Atoi(y[1])
	startZ, _ := strconv.Atoi(z[0][2:])
	endZ, _ := strconv.Atoi(z[1])

	return Cube{position, startX, endX, startY, endY, startZ, endZ, 0}
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	var cubes []Cube
	for _, line := range lines {
		newCube := buildCube(line)
		cubes = append(cubes, newCube)
	}

	var adjustedCubes []Cube
	for _, cube := range cubes {
		var newAdjustedCubes []Cube
		cube.value = (cube.endX - cube.startX + 1) * (cube.endY - cube.startY + 1) * (cube.endZ - cube.startZ + 1)
		for _, otherCube := range adjustedCubes {
			newCube := cube.GetIntersect(otherCube)
			if newCube == nil {
				continue
			}
			if cube.position == "on" && newCube.position == "on" {
				newCube.position = "off"
			} else if cube.position == "off" && newCube.position == "off" {
				newCube.position = "on"
			} else {
				newCube.position = cube.position
			}
			newAdjustedCubes = append(newAdjustedCubes, *newCube)
		}
		if cube.position == "on" {
			adjustedCubes = append(adjustedCubes, cube)
		}
		adjustedCubes = append(adjustedCubes, newAdjustedCubes...)
	}

	var count int
	for _, cube := range adjustedCubes {
		if cube.position == "on" {
			count += cube.value
		} else {
			count -= cube.value
		}
	}
	fmt.Println(count)
}
