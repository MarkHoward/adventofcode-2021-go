package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func countBits(numbers []int64) int64 {
	var bitChecker int64 = 0b100000000000
	var result int64

	for bitChecker != 0 {
		var count int
		for _, num := range numbers {
			if num&bitChecker != 0 {
				count++
			}
		}
		if count >= len(numbers)/2 {
			result += bitChecker
		}
		bitChecker = bitChecker >> 1
	}
	return result
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []int64
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		num, err := strconv.ParseInt(scanner.Text(), 2, 16)
		if err != nil {
			fmt.Println("Error parsing input.txt")
		}
		lines = append(lines, num)
	}

	gammaRate := countBits(lines)
	fmt.Println(gammaRate * (gammaRate ^ 0b111111111111))
}
