// Had to look up the answer on this one more or less
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Area struct {
	startX int
	endX   int
	startY int
	endY   int
}

func parseTargetArea(input string) Area {
	parts := strings.Split(input, ",")
	x := strings.Split(parts[0], "=")
	y := strings.Split(parts[1], "=")
	x = strings.Split(x[1], "..")
	y = strings.Split(y[1], "..")

	startX, err := strconv.Atoi(x[0])
	if err != nil {
		panic(err)
	}
	endX, err := strconv.Atoi(x[1])
	if err != nil {
		panic(err)
	}
	startY, err := strconv.Atoi(y[0])
	if err != nil {
		panic(err)
	}
	endY, err := strconv.Atoi(y[1])
	if err != nil {
		panic(err)
	}

	return Area{startX, endX, startY, endY}
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()
	targetArea := parseTargetArea(input)
	n := -targetArea.startY - 1
	fmt.Println(n * (n + 1) / 2)
}
