package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strings"
)

func getPixel(input string, algorithm string) string {
	var idx int
	for i := len(input) - 1; i >= 0; i-- {
		if input[len(input)-i-1] == '#' {
			idx += int(math.Pow(2, float64(i)))
		}
	}
	return string(rune(algorithm[idx]))
}

func getChar(row int, col int, image []string, defaultValue string) string {
	if row < 0 {
		return defaultValue
	}
	if col < 0 {
		return defaultValue
	}
	if row >= len(image) {
		return defaultValue
	}
	if col >= len(image[0]) {
		return defaultValue
	}
	return string(rune(image[row][col]))
}

func buildImage(image []string, algorithm string, defaultValue string) []string {
	var newImage []string

	for i := 0; i < len(image); i++ {
		newImage = append(newImage, "")
		for j := 0; j < len(image[i]); j++ {
			input := getChar(i-1, j-1, image, defaultValue)
			input += getChar(i-1, j, image, defaultValue)
			input += getChar(i-1, j+1, image, defaultValue)
			input += getChar(i, j-1, image, defaultValue)
			input += getChar(i, j, image, defaultValue)
			input += getChar(i, j+1, image, defaultValue)
			input += getChar(i+1, j-1, image, defaultValue)
			input += getChar(i+1, j, image, defaultValue)
			input += getChar(i+1, j+1, image, defaultValue)
			newImage[i] += getPixel(input, algorithm)
		}
	}

	return newImage
}

func padImage(image []string) []string {
	for i := range image {
		image[i] = "......................................................................" + image[i] + "......................................................................"
	}
	var emptyRow string
	for range image[0] {
		emptyRow += "."
	}
	newImage := make([]string, 60)
	for i := range newImage {
		newImage[i] = emptyRow
	}
	return append(newImage, append(image, newImage...)...)
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	algorithm := lines[0]
	image := lines[2:]
	image = padImage(image)

	for i := 0; i < 25; i++ {
		image = buildImage(image, algorithm, ".")
		image = buildImage(image, algorithm, "#")
	}
	// image = buildImage(image, algorithm, ".")
	// for _, line := range image {
	// 	fmt.Println(line)
	// }
	// fmt.Println("----------")

	// image = buildImage(image, algorithm, "#")
	// for _, line := range image {
	// 	fmt.Println(line)
	// }

	fmt.Println(strings.Count(strings.Join(image, ""), "#"))
}
