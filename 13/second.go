package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func buildGrid(input []string) [][]bool {
	var largestRow, largestCol int
	points := make(map[int][]int)
	for _, line := range input {
		coords := strings.Split(line, ",")
		col, err := strconv.Atoi(coords[0])
		if err != nil {
			fmt.Println(err)
		}
		row, err := strconv.Atoi(coords[1])
		if err != nil {
			fmt.Println(err)
		}
		if _, ok := points[row]; !ok {
			points[row] = make([]int, 0)
		}
		points[row] = append(points[row], col)
		if row > largestRow {
			largestRow = row
		}
		if col > largestCol {
			largestCol = col
		}
	}

	grid := make([][]bool, largestRow+1)
	for i := range grid {
		grid[i] = make([]bool, largestCol+1)
	}

	for rowIdx, row := range points {
		for _, col := range row {
			grid[rowIdx][col] = true
		}
	}

	return grid
}

func foldY(grid [][]bool, input string) [][]bool {
	targetIdx, err := strconv.Atoi(strings.Split(input, "=")[1])
	if err != nil {
		fmt.Println(err)
	}

	for rowIdx, row := range grid[targetIdx+1:] {
		for colIdx, col := range row {
			grid[targetIdx-rowIdx-1][colIdx] = col || grid[targetIdx-rowIdx-1][colIdx]
		}
	}

	return grid[:targetIdx]
}

func foldX(grid [][]bool, input string) [][]bool {
	targetIdx, err := strconv.Atoi(strings.Split(input, "=")[1])
	if err != nil {
		fmt.Println(err)
	}

	for rowIdx, row := range grid {
		for colIdx, col := range row[targetIdx+1:] {
			grid[rowIdx][targetIdx-colIdx-1] = col || grid[rowIdx][targetIdx-colIdx-1]
		}
		grid[rowIdx] = grid[rowIdx][:targetIdx]
	}

	return grid
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var gridLines, instructionLines []string
	delimiterFound := false
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			delimiterFound = true
			continue
		}
		if delimiterFound {
			instructionLines = append(instructionLines, line)
		} else {
			gridLines = append(gridLines, line)
		}
	}
	grid := buildGrid(gridLines)
	for _, instruction := range instructionLines {
		if strings.Split(instruction, " ")[2][0] == 'x' {
			grid = foldX(grid, instruction)
		} else {
			grid = foldY(grid, instruction)
		}
	}

	for _, row := range grid {
		text := make([]string, len(row))
		for idx, col := range row {
			if col {
				text[idx] = "#"
			} else {
				text[idx] = "."
			}
		}
		fmt.Println(text)
	}
}
