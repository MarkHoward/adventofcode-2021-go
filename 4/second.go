package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type row struct {
	numbers []int
}

type board struct {
	rows []row
}

func calculateScore(winningBoard board, finalNumber int) int {
	var sum int
	for _, row := range winningBoard.rows {
		for _, number := range row.numbers {
			sum += number
		}
	}
	return sum * finalNumber
}

func checkWinner(boardToCheck board) bool {
	var columnCounts [5]int
	var rowCounts [5]int
	for rowIndex, row := range boardToCheck.rows {
		for colIndex, col := range row.numbers {
			if col == 0 {
				rowCounts[rowIndex]++
				columnCounts[colIndex]++
			}
		}
	}
	for _, v := range columnCounts {
		if v >= 5 {
			return true
		}
	}
	for _, v := range rowCounts {
		if v >= 5 {
			return true
		}
	}
	return false
}

func checkBoards(boards *[]board, num int) int {
	for boardIndex, board := range *boards {
		for rowIndex, row := range board.rows {
			for numberIndex, number := range row.numbers {
				if number == num {
					(*boards)[boardIndex].rows[rowIndex].numbers[numberIndex] = 0
					if checkWinner((*boards)[boardIndex]) {
						return boardIndex
					}
				}
			}
		}
	}
	return -1
}

func getLastWinner(boards []board, draws string) (board, int) {
	for _, num := range strings.Split(draws, ",") {
		num, err := strconv.Atoi(num)
		if err != nil {
			continue
		}
		for true {
			index := checkBoards(&boards, num)
			if index != -1 {
				if len(boards) == 1 {
					return boards[0], num
				}
				boards = append(boards[:index], boards[index+1:]...)
			} else {
				break
			}
		}

	}
	return boards[0], 0
}

func parseBoards(input []string) []board {
	var currentBoard board
	var boards []board
	for _, line := range input {
		if line == "" {
			boards = append(boards, currentBoard)
			currentBoard.rows = make([]row, 0)
			continue
		}
		var currentRow row
		for _, entry := range strings.Split(line, " ") {
			num, err := strconv.Atoi(entry)
			if err != nil {
				continue
			}
			currentRow.numbers = append(currentRow.numbers, num)
		}
		currentBoard.rows = append(currentBoard.rows, currentRow)
	}
	return boards
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	scanner.Scan()
	draws := scanner.Text()
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	boards := parseBoards(lines)
	finalBoard, finalNumber := getLastWinner(boards, draws)
	score := calculateScore(finalBoard, finalNumber)
	fmt.Println(score)
}
