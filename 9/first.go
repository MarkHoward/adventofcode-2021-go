package main

import (
	"bufio"
	"fmt"
	"os"
)

func isLowPoint(point int, adjacent []int) bool {
	for _, adj := range adjacent {
		if point >= adj {
			return false
		}
	}
	return true
}

func getDanger(points [][]int) int {
	var count int

	for rowIdx := range points {
		for colIdx := range points[rowIdx] {
			var adjacent []int
			if colIdx > 0 {
				adjacent = append(adjacent, points[rowIdx][colIdx-1])
			}
			if rowIdx > 0 {
				adjacent = append(adjacent, points[rowIdx-1][colIdx])
			}
			if colIdx < len(points[rowIdx])-1 {
				adjacent = append(adjacent, points[rowIdx][colIdx+1])
			}
			if rowIdx < len(points)-1 {
				adjacent = append(adjacent, points[rowIdx+1][colIdx])
			}
			if isLowPoint(points[rowIdx][colIdx], adjacent) {
				count += points[rowIdx][colIdx] + 1
			}
		}
	}

	return count
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var numbers [][]int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		numRunes := []rune(scanner.Text())
		var numArray []int
		for _, num := range numRunes {
			numArray = append(numArray, int(num-'0'))
		}
		numbers = append(numbers, numArray)
	}
	dangerCount := getDanger(numbers)
	fmt.Println(dangerCount)
}
