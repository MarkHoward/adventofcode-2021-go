package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Die struct {
	timesRolled int
}

func (d *Die) Roll() int {
	(*d).timesRolled++
	result := d.timesRolled % 100
	if result == 0 {
		result = 100
	}
	return result
}

type Player struct {
	points       int
	currentSpace int
}

func (p *Player) TakeTurn(die *Die) {
	movement := die.Roll()
	movement += die.Roll()
	movement += die.Roll()

	newSpace := (p.currentSpace + movement) % 10
	if newSpace == 0 {
		newSpace = 10
	}
	(*p).points += newSpace
	(*p).currentSpace = newSpace
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	starting1, err := strconv.Atoi(strings.Split(lines[0], ": ")[1])
	if err != nil {
		panic(err)
	}
	starting2, err := strconv.Atoi(strings.Split(lines[1], ": ")[1])
	if err != nil {
		panic(err)
	}

	player1 := Player{0, starting1}
	player2 := Player{0, starting2}
	die := Die{0}

	for true {
		player1.TakeTurn(&die)
		if player1.points >= 1000 {
			break
		}
		player2.TakeTurn(&die)
		if player2.points >= 1000 {
			break
		}
	}
	if player1.points > player2.points {
		fmt.Println(player2.points * die.timesRolled)
	} else {
		fmt.Println(player1.points * die.timesRolled)
	}
}
