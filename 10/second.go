package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"sort"
)

type Stack []rune

func (s Stack) IsEmpty() bool {
	return len(s) == 0
}

func (s *Stack) Push(str rune) {
	*s = append(*s, str)
}

func (s *Stack) Pop() (rune, error) {
	if s.IsEmpty() {
		return rune(0), errors.New("empty stack")
	}
	index := len(*s) - 1
	element := (*s)[index]
	*s = (*s)[:index]
	return element, nil
}

func (s *Stack) Peek() rune {
	if s.IsEmpty() {
		return rune(0)
	}
	return (*s)[len(*s)-1]
}

func isClosingChar(char rune) bool {
	return char == '>' || char == '}' || char == ']' || char == ')'
}

func isCorrespondingChar(closingChar rune, stackChar rune) bool {
	switch closingChar {
	case '>':
		return stackChar == '<'
	case '}':
		return stackChar == '{'
	case ']':
		return stackChar == '['
	case ')':
		return stackChar == '('
	default:
		return false
	}
}

func parseChunks(lines []string) ([]string, []string) {
	var incomplete, invalid []string

	for _, line := range lines {
		var stack Stack
		invalidLine := false
		for _, char := range line {
			if isClosingChar(char) {
				if isCorrespondingChar(char, stack.Peek()) {
					stack.Pop()
				} else {
					invalidLine = true
					invalid = append(invalid, line)
					break
				}
			} else {
				stack.Push(char)
			}
		}
		if !invalidLine && !stack.IsEmpty() {
			incomplete = append(incomplete, line)
		}
	}

	return incomplete, invalid
}

func getLineScores(incomplete []string) []int {
	var scores []int

	for _, line := range incomplete {
		var stack Stack
		for _, char := range line {
			if isClosingChar(char) {
				stack.Pop()
			} else {
				stack.Push(char)
			}
		}
		var score int
		for !stack.IsEmpty() {
			char, err := stack.Pop()
			if err != nil {
				fmt.Println(err)
			}
			score *= 5
			switch char {
			case '(':
				score += 1
			case '[':
				score += 2
			case '{':
				score += 3
			case '<':
				score += 4
			default:
				fmt.Println("Invalid character", string(char))
			}
		}
		scores = append(scores, score)
	}

	return scores
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	incomplete, _ := parseChunks(lines)
	scores := getLineScores(incomplete)
	sort.Ints(scores)
	fmt.Println(scores[len(scores)/2])
}
