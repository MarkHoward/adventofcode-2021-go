package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func calculateCost(positions []int, position int) int {
	var cost int
	for _, pos := range positions {
		thisCost := pos - position
		if thisCost < 0 {
			thisCost = -thisCost
		}
		cost += thisCost
	}
	return cost
}

func findCheapestFuel(positions []int) int {
	var cost int
	minPos := positions[0]
	maxPos := positions[len(positions)-1]
	startingPos := positions[len(positions)/2]
	cost = calculateCost(positions, startingPos)

	for startingPos < maxPos {
		if newCost := calculateCost(positions, startingPos+1); newCost <= cost {
			cost = newCost
			startingPos++
		} else {
			break
		}
	}
	for startingPos > minPos {
		if newCost := calculateCost(positions, startingPos-1); newCost <= cost {
			cost = newCost
			startingPos--
		} else {
			break
		}
	}

	return cost
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()

	var numbers []int
	for _, num := range strings.Split(input, ",") {
		number, err := strconv.Atoi(num)
		if err != nil {
			fmt.Println("Error converting number", num)
		}
		numbers = append(numbers, number)
	}
	sort.Ints(numbers)
	cost := findCheapestFuel(numbers)
	fmt.Println(cost)
}
