package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type Packet struct {
	version    int64
	typeId     int64
	value      int64
	subPackets []*Packet
}

func sumValue(packets []*Packet) int64 {
	var sum int64

	for _, packet := range packets {
		sum += packet.value
	}

	return sum
}

func productValue(packets []*Packet) int64 {
	var product int64 = 1

	for _, packet := range packets {
		product *= packet.value
	}

	return product
}

func minimumValue(packets []*Packet) int64 {
	minimum := packets[0].value

	for _, packet := range packets {
		if minimum > packet.value {
			minimum = packet.value
		}
	}

	return minimum
}

func maximumValue(packets []*Packet) int64 {
	maximum := packets[0].value

	for _, packet := range packets {
		if maximum < packet.value {
			maximum = packet.value
		}
	}

	return maximum
}

func greaterValue(packets []*Packet) int64 {
	if packets[0].value > packets[1].value {
		return 1
	}
	return 0
}

func lesserValue(packets []*Packet) int64 {
	if packets[0].value < packets[1].value {
		return 1
	}
	return 0
}

func equalValue(packets []*Packet) int64 {
	if packets[0].value == packets[1].value {
		return 1
	}
	return 0
}

func literalValue(binary string) (int64, int64) {
	var startBitIdx int64
	var adjustedBinary string
	for true {
		startingBit := rune(binary[startBitIdx])
		adjustedBinary = adjustedBinary + binary[startBitIdx+1:startBitIdx+5]
		if startingBit == '0' {
			break
		}
		startBitIdx += 5
	}
	value, err := strconv.ParseInt(adjustedBinary, 2, 64)
	if err != nil {
		panic(err)
	}
	return value, startBitIdx + 5
}

func buildPacket(binary string) (Packet, int64) {
	version, err := strconv.ParseInt(binary[:3], 2, 64)
	var subPackets []*Packet
	var bitsUsed, packetValue int64
	if err != nil {
		panic(err)
	}
	typeId, err := strconv.ParseInt(binary[3:6], 2, 64)
	if err != nil {
		panic(err)
	}
	if typeId == 4 {
		value, endIdx := literalValue(binary[6:])
		packetValue = value
		bitsUsed = endIdx + 6
	} else if binary[6] == '0' {
		subLength, err := strconv.ParseInt(binary[7:22], 2, 64)
		if err != nil {
			panic(err)
		}
		var subIdx int64
		for subIdx < subLength {
			subPacket, endIdx := buildPacket(binary[subIdx+22:])
			subPackets = append(subPackets, &subPacket)
			subIdx += endIdx
		}
		bitsUsed = subLength + 22
	} else if binary[6] == '1' {
		subLength, err := strconv.ParseInt(binary[7:18], 2, 64)
		if err != nil {
			panic(err)
		}
		var lengthUsed int64
		for subLength > 0 {
			subPacket, endIdx := buildPacket(binary[18+lengthUsed:])
			subPackets = append(subPackets, &subPacket)
			subLength--
			lengthUsed += endIdx
		}
		bitsUsed = lengthUsed + 18
	}
	switch typeId {
	case 0:
		packetValue = sumValue(subPackets)
	case 1:
		packetValue = productValue(subPackets)
	case 2:
		packetValue = minimumValue(subPackets)
	case 3:
		packetValue = maximumValue(subPackets)
	case 5:
		packetValue = greaterValue(subPackets)
	case 6:
		packetValue = lesserValue(subPackets)
	case 7:
		packetValue = equalValue(subPackets)
	}
	return Packet{version, typeId, packetValue, subPackets}, bitsUsed
}

func convertToBinary(input string) string {
	conversions := map[rune]string{
		'0': "0000",
		'1': "0001",
		'2': "0010",
		'3': "0011",
		'4': "0100",
		'5': "0101",
		'6': "0110",
		'7': "0111",
		'8': "1000",
		'9': "1001",
		'A': "1010",
		'B': "1011",
		'C': "1100",
		'D': "1101",
		'E': "1110",
		'F': "1111",
	}
	var result string
	for _, char := range input {
		result = result + conversions[char]
	}
	return result
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()
	binary := convertToBinary(input)
	packet, _ := buildPacket(binary)
	fmt.Println(packet.value)
}
