package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func buildInstructions(input []string) map[string]string {
	instructions := make(map[string]string)

	for _, line := range input {
		instruction := strings.Split(line, " -> ")
		instructions[instruction[0]] = instruction[1]
	}

	return instructions
}

func runInsertions(polymer string, instructions map[string]string, numOfSteps int) string {
	for i := 0; i < numOfSteps; i++ {
		newPolymer := polymer

		offset := 1
		for j := 0; j < len(polymer)-1; j++ {
			if char, ok := instructions[polymer[j:j+2]]; ok {
				newPolymer = newPolymer[:j+offset] + char + newPolymer[j+offset:]
				offset++
			}
		}

		polymer = newPolymer
	}
	return polymer
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	polymer := lines[0]
	instructions := buildInstructions(lines[2:])
	polymer = runInsertions(polymer, instructions, 10)
	charCounts := make(map[rune]int)
	for _, char := range polymer {
		if _, ok := charCounts[char]; ok {
			charCounts[char]++
		} else {
			charCounts[char] = 1
		}
	}

	var highCount, lowCount int
	for _, count := range charCounts {
		if count > highCount {
			highCount = count
		}
		if count < lowCount || lowCount == 0 {
			lowCount = count
		}
	}

	fmt.Println(highCount - lowCount)
}
