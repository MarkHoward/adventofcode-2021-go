// Needed a clarification hint on this one
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type Packet struct {
	version    int64
	typeId     int64
	value      int64
	subPackets []*Packet
}

func addPacketVersions(packet Packet) int64 {
	if len(packet.subPackets) == 0 {
		return packet.version
	}
	value := packet.version
	for _, subpacket := range packet.subPackets {
		value += addPacketVersions(*subpacket)
	}
	return value
}

func getPacketValue(binary string) (int64, int64) {
	var startBitIdx int64
	var adjustedBinary string
	for true {
		startingBit := rune(binary[startBitIdx])
		adjustedBinary = adjustedBinary + binary[startBitIdx+1:startBitIdx+5]
		if startingBit == '0' {
			break
		}
		startBitIdx += 5
	}
	value, err := strconv.ParseInt(adjustedBinary, 2, 64)
	if err != nil {
		panic(err)
	}
	return value, startBitIdx + 5
}

func buildPacket(binary string) (Packet, int64) {
	version, err := strconv.ParseInt(binary[:3], 2, 64)
	var subPackets []*Packet
	if err != nil {
		panic(err)
	}
	typeId, err := strconv.ParseInt(binary[3:6], 2, 64)
	if err != nil {
		panic(err)
	}
	if typeId == 4 {
		value, endIdx := getPacketValue(binary[6:])
		return Packet{version, typeId, value, subPackets}, endIdx + 6
	}
	if binary[6] == '0' {
		subLength, err := strconv.ParseInt(binary[7:22], 2, 64)
		if err != nil {
			panic(err)
		}
		var subIdx int64
		for subIdx < subLength {
			subPacket, endIdx := buildPacket(binary[subIdx+22:])
			subPackets = append(subPackets, &subPacket)
			subIdx += endIdx
		}
		return Packet{version, typeId, 0, subPackets}, subLength + 22
	}
	if binary[6] == '1' {
		subLength, err := strconv.ParseInt(binary[7:18], 2, 64)
		if err != nil {
			panic(err)
		}
		var lengthUsed int64
		for subLength > 0 {
			subPacket, endIdx := buildPacket(binary[18+lengthUsed:])
			subPackets = append(subPackets, &subPacket)
			subLength--
			lengthUsed += endIdx
		}
		return Packet{version, typeId, 0, subPackets}, lengthUsed + 18
	}
	panic("something went wrong")
}

func convertToBinary(input string) string {
	conversions := map[rune]string{
		'0': "0000",
		'1': "0001",
		'2': "0010",
		'3': "0011",
		'4': "0100",
		'5': "0101",
		'6': "0110",
		'7': "0111",
		'8': "1000",
		'9': "1001",
		'A': "1010",
		'B': "1011",
		'C': "1100",
		'D': "1101",
		'E': "1110",
		'F': "1111",
	}
	var result string
	for _, char := range input {
		result = result + conversions[char]
	}
	return result
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()
	binary := convertToBinary(input)
	packet, _ := buildPacket(binary)
	value := addPacketVersions(packet)
	fmt.Println(value)
}
