package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
)

type Stack []rune

func (s Stack) IsEmpty() bool {
	return len(s) == 0
}

func (s *Stack) Push(str rune) {
	*s = append(*s, str)
}

func (s *Stack) Pop() (rune, error) {
	if s.IsEmpty() {
		return rune(0), errors.New("empty stack")
	}
	index := len(*s) - 1
	element := (*s)[index]
	*s = (*s)[:index]
	return element, nil
}

func (s *Stack) Peek() rune {
	if s.IsEmpty() {
		return rune(0)
	}
	return (*s)[len(*s)-1]
}

func isClosingChar(char rune) bool {
	return char == '>' || char == '}' || char == ']' || char == ')'
}

func isCorrespondingChar(closingChar rune, stackChar rune) bool {
	switch closingChar {
	case '>':
		return stackChar == '<'
	case '}':
		return stackChar == '{'
	case ']':
		return stackChar == '['
	case ')':
		return stackChar == '('
	default:
		return false
	}
}

func parseChunks(lines []string) ([]string, []string) {
	var incomplete, invalid []string

	for _, line := range lines {
		var stack Stack
		invalidLine := false
		for _, char := range line {
			if isClosingChar(char) {
				if isCorrespondingChar(char, stack.Peek()) {
					stack.Pop()
				} else {
					invalidLine = true
					invalid = append(invalid, line)
					break
				}
			} else {
				stack.Push(char)
			}
		}
		if !invalidLine && !stack.IsEmpty() {
			incomplete = append(incomplete, line)
		}
	}

	return incomplete, invalid
}

func getFirstIllegalChar(invalid []string) []rune {
	var chars []rune

	for _, line := range invalid {
		var stack Stack
		for _, char := range line {
			if isClosingChar(char) {
				if isCorrespondingChar(char, stack.Peek()) {
					stack.Pop()
				} else {
					chars = append(chars, char)
					break
				}
			} else {
				stack.Push(char)
			}
		}
	}
	return chars
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	_, invalid := parseChunks(lines)
	chars := getFirstIllegalChar(invalid)

	var count int
	for _, char := range chars {
		switch char {
		case ')':
			count += 3
		case ']':
			count += 57
		case '}':
			count += 1197
		case '>':
			count += 25137
		default:
			fmt.Println("Invalid character", string(char))
		}
	}
	fmt.Println(count)
}
