package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Area struct {
	startX int
	endX   int
	startY int
	endY   int
}

type Probe struct {
	xPos int
	yPos int
	xVel int
	yVel int
}

type Result int

const (
	OnTarget Result = iota
	Miss
	Moving
)

func (p *Probe) Step() {
	(*p).xPos += p.xVel
	(*p).yPos += p.yVel
	if p.xVel > 0 {
		(*p).xVel--
	} else if p.xVel < 0 {
		(*p).xVel++
	}
	(*p).yVel--
}

func (a Area) CheckProbe(p Probe) Result {
	if p.xPos >= a.startX && p.xPos <= a.endX && p.yPos >= a.startY && p.yPos <= a.endY {
		return OnTarget
	}
	if p.xVel == 0 {
		if p.xPos > a.endX || p.xPos < a.startX {
			return Miss
		}
	}
	if p.yVel <= 0 {
		if p.yPos < a.startY {
			return Miss
		}
	}
	return Moving
}

func launchProbe(targetArea Area, x int, y int) Result {
	probe := Probe{0, 0, x, y}
	result := targetArea.CheckProbe(probe)
	for result == Moving {
		probe.Step()
		result = targetArea.CheckProbe(probe)
	}
	return result
}

func findSuccessfulProbes(targetArea Area) int {
	var count int
	for x := 1; x <= targetArea.endX; x++ {
		for y := targetArea.startY; y <= -targetArea.startY; y++ {
			result := launchProbe(targetArea, x, y)
			if result == OnTarget {
				count++
			}
		}
	}
	return count
}

func parseTargetArea(input string) Area {
	parts := strings.Split(input, ",")
	x := strings.Split(parts[0], "=")
	y := strings.Split(parts[1], "=")
	x = strings.Split(x[1], "..")
	y = strings.Split(y[1], "..")

	startX, err := strconv.Atoi(x[0])
	if err != nil {
		panic(err)
	}
	endX, err := strconv.Atoi(x[1])
	if err != nil {
		panic(err)
	}
	startY, err := strconv.Atoi(y[0])
	if err != nil {
		panic(err)
	}
	endY, err := strconv.Atoi(y[1])
	if err != nil {
		panic(err)
	}

	return Area{startX, endX, startY, endY}
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()
	targetArea := parseTargetArea(input)
	count := findSuccessfulProbes(targetArea)
	fmt.Println(count)
}
