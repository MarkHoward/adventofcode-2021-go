// Had to get a hint on this one
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getFish(lanternFish map[int]int) int {
	var count int
	days := 256
	for days > 0 {
		days--
		newFish := lanternFish[0]
		for i := 0; i < 8; i++ {
			lanternFish[i] = lanternFish[i+1]
		}
		lanternFish[8] = newFish
		lanternFish[6] += newFish
	}
	for _, fish := range lanternFish {
		count += fish
	}
	return count
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()

	lanternFish := make(map[int]int)

	for _, num := range strings.Split(input, ",") {
		number, err := strconv.Atoi(num)
		if err != nil {
			fmt.Println("Error converting number", num)
		}
		if _, ok := lanternFish[number]; ok {
			lanternFish[number]++
		} else {
			lanternFish[number] = 1
		}
	}

	fish := getFish(lanternFish)
	fmt.Println(fish)
}
