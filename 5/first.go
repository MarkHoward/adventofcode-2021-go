package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type position struct {
	x int
	y int
}

type ventLine struct {
	start position
	end   position
}

func countDangerous(grid [1000][1000]int) int {
	var count int
	for x := range grid {
		for _, y := range grid[x] {
			if y >= 2 {
				count++
			}
		}
	}
	return count
}

func buildGrid(horizontal []ventLine, vertical []ventLine) [1000][1000]int {
	var grid [1000][1000]int

	var startX, startY, endX, endY int

	for _, line := range horizontal {
		if line.start.x > line.end.x {
			startX = line.end.x
			endX = line.start.x
		} else {
			startX = line.start.x
			endX = line.end.x
		}

		for startX <= endX {
			grid[startX][line.start.y]++
			startX++
		}
	}
	for _, line := range vertical {
		if line.start.y > line.end.y {
			startY = line.end.y
			endY = line.start.y
		} else {
			startY = line.start.y
			endY = line.end.y
		}
		for startY <= endY {
			grid[line.start.x][startY]++
			startY++
		}
	}

	return grid
}

func getLines(input []string) ([]ventLine, []ventLine) {
	var horizontal, vertical []ventLine
	for _, line := range input {
		positions := strings.Split(line, " -> ")
		start := strings.Split(positions[0], ",")
		end := strings.Split(positions[1], ",")
		startX, err1 := strconv.Atoi(start[0])
		startY, err2 := strconv.Atoi(start[1])
		endX, err3 := strconv.Atoi(end[0])
		endY, err4 := strconv.Atoi(end[1])
		vent := ventLine{position{startX, startY}, position{endX, endY}}
		if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
			fmt.Println("Error parsing position")
		}
		if startX == endX {
			vertical = append(vertical, vent)
		}
		if startY == endY {
			horizontal = append(horizontal, vent)
		}
	}
	return horizontal, vertical
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	horizontal, vertical := getLines(lines)
	grid := buildGrid(horizontal, vertical)
	dangerCount := countDangerous(grid)
	fmt.Println(dangerCount)
}
