package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func spawnFish(lanternFish []int) []int {
	days := 80
	for days > 0 {
		lanternFishCopy := make([]int, len(lanternFish))
		copy(lanternFishCopy, lanternFish)
		for idx := range lanternFishCopy {
			lanternFish[idx]--
			if lanternFish[idx] < 0 {
				lanternFish[idx] = 6
				lanternFish = append(lanternFish, 8)
			}
		}
		days--
	}
	return lanternFish
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()

	var numbers []int
	for _, num := range strings.Split(input, ",") {
		number, err := strconv.Atoi(num)
		if err != nil {
			fmt.Println("Error converting number", num)
		}
		numbers = append(numbers, number)
	}

	fish := spawnFish(numbers)
	fmt.Println(len(fish))
}
