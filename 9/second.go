package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

type point struct {
	x int
	y int
}

func getAdjacentPoints(grid [][]int, pt point) []point {
	var points []point

	if pt.x > 0 {
		points = append(points, point{x: pt.x - 1, y: pt.y})
	}
	if pt.y > 0 {
		points = append(points, point{x: pt.x, y: pt.y - 1})
	}
	if pt.x < len(grid)-1 {
		points = append(points, point{x: pt.x + 1, y: pt.y})
	}
	if pt.y < len(grid[pt.x])-1 {
		points = append(points, point{x: pt.x, y: pt.y + 1})
	}

	return points
}

func containsPoint(points []point, pt point) bool {
	for i := range points {
		if points[i] == pt {
			return true
		}
	}
	return false
}

func buildBasins(grid [][]int, lowPoints []point) [][]int {
	var basins [][]int

	for _, pt := range lowPoints {
		var basin []int
		var checkedPoints, pointsToCheck []point

		basin = append(basin, grid[pt.x][pt.y])
		checkedPoints = append(checkedPoints, pt)
		pointsToCheck = append(pointsToCheck, getAdjacentPoints(grid, pt)...)

		for len(pointsToCheck) > 0 {
			if !containsPoint(checkedPoints, pointsToCheck[0]) {
				if grid[pointsToCheck[0].x][pointsToCheck[0].y] != 9 {
					pointsToCheck = append(pointsToCheck, getAdjacentPoints(grid, pointsToCheck[0])...)
					basin = append(basin, grid[pointsToCheck[0].x][pointsToCheck[0].y])
				}
				checkedPoints = append(checkedPoints, pointsToCheck[0])
			}
			pointsToCheck = pointsToCheck[1:]
		}
		basins = append(basins, basin)
	}

	return basins
}

func isLowPoint(point int, adjacent []int) bool {
	for _, adj := range adjacent {
		if point >= adj {
			return false
		}
	}
	return true
}

func findLowPoints(grid [][]int) []point {
	var points []point

	for rowIdx := range grid {
		for colIdx := range grid[rowIdx] {
			var adjacent []int
			if colIdx > 0 {
				adjacent = append(adjacent, grid[rowIdx][colIdx-1])
			}
			if rowIdx > 0 {
				adjacent = append(adjacent, grid[rowIdx-1][colIdx])
			}
			if colIdx < len(grid[rowIdx])-1 {
				adjacent = append(adjacent, grid[rowIdx][colIdx+1])
			}
			if rowIdx < len(grid)-1 {
				adjacent = append(adjacent, grid[rowIdx+1][colIdx])
			}
			if isLowPoint(grid[rowIdx][colIdx], adjacent) {
				points = append(points, point{rowIdx, colIdx})
			}
		}
	}

	return points
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var grid [][]int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		numRunes := []rune(scanner.Text())
		var numArray []int
		for _, num := range numRunes {
			numArray = append(numArray, int(num-'0'))
		}
		grid = append(grid, numArray)
	}
	lowPoints := findLowPoints(grid)
	basins := buildBasins(grid, lowPoints)
	sort.Slice(basins, func(i, j int) bool {
		return len(basins[i]) > len(basins[j])
	})
	fmt.Println(len(basins[0]) * len(basins[1]) * len(basins[2]))
}
