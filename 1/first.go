package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func convertToIntArray(input []string) []int {
	var result []int
	for _, val := range input {
		num, err := strconv.Atoi(val)
		if err != nil {
			fmt.Println("Error converting", val)
			continue
		}
		result = append(result, num)
	}
	return result
}

func calculateDepthChanges(depths []int) int {
	var counter int
	for idx, depth := range depths {
		if idx == 0 {
			continue
		}
		if depth > depths[idx-1] {
			counter++
		}
	}
	return counter
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	fmt.Println(calculateDepthChanges(convertToIntArray(lines)))
}
