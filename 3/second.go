package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func filter(array []int64, callback func(int64) bool) []int64 {
	var result []int64
	for _, item := range array {
		if callback(item) {
			result = append(result, item)
		}
	}
	return result
}

func getRating(numbers []int64, checker int) int64 {
	var bitChecker int64 = 0b100000000000

	for bitChecker != 0 && len(numbers) > 1 {
		var count int
		for _, num := range numbers {
			if num&bitChecker != 0 {
				count++
			}
		}
		if checker == 0 {
			numbers = filter(numbers, func(num int64) bool {
				if count < (len(numbers)+1)/2 {
					return num&bitChecker > 0
				} else {
					return num&bitChecker == 0
				}
			})
		} else {
			numbers = filter(numbers, func(num int64) bool {
				if count >= (len(numbers)+1)/2 {
					return num&bitChecker > 0
				} else {
					return num&bitChecker == 0
				}
			})
		}
		bitChecker = bitChecker >> 1
	}
	return numbers[0]
}

func getRatings(numbers []int64) (int64, int64) {
	generatorRating := getRating(numbers, 1)
	scrubberRating := getRating(numbers, 0)

	return generatorRating, scrubberRating
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []int64
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		num, err := strconv.ParseInt(scanner.Text(), 2, 16)
		if err != nil {
			fmt.Println("Error parsing input.txt")
		}
		lines = append(lines, num)
	}

	generatorRating, scrubberRating := getRatings(lines)
	fmt.Println(generatorRating * scrubberRating)
}
