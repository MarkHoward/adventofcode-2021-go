// This one has an off by one error that I can't find :(
// Gets the correct answer on the example, is one too high on the input
// Needed a hint on this one as well
package main

import (
	"bufio"
	"fmt"
	"os"
)

type Node struct {
	value int
	edges []Edge
	next  *Node
}

type Edge struct {
	a, b  *Node
	value int
}

func findPath(currentNode *Node) int {
	if currentNode.next == nil {
		return currentNode.value
	}

	for _, edge := range currentNode.edges {
		if currentNode.value+edge.value < edge.b.value {
			(*edge.b).value = currentNode.value + edge.value
		}
	}

	return findPath(currentNode.next)
}

func buildGraph(input []string) *Node {
	const maxInt = 0b111111111111111111111111111111111111111111111111111111111111111
	var nodes []*Node
	var currentNode *Node = nil

	for i := 0; i < (len(input) * len(input[0])); i++ {
		newNode := Node{maxInt, []Edge{}, nil}
		if i == 0 {
			newNode.value = 0
		}
		nodes = append(nodes, &newNode)
		if currentNode == nil {
			currentNode = &newNode
		} else {
			(*currentNode).next = &newNode
			currentNode = (*currentNode).next
		}
	}

	for rowIdx := range input {
		for colIdx := range input[rowIdx] {
			thisNode := nodes[rowIdx*len(input)+colIdx]
			if colIdx > 0 {
				thatNode := nodes[rowIdx*len(input)+colIdx-1]
				num := int(rune(input[rowIdx][colIdx-1]) - '0')
				edge := Edge{thisNode, thatNode, num}
				(*thisNode).edges = append((*thisNode).edges, edge)
			}
			if colIdx < len(input[rowIdx])-1 {
				thatNode := nodes[rowIdx*len(input)+colIdx+1]
				num := int(rune(input[rowIdx][colIdx+1]) - '0')
				edge := Edge{thisNode, thatNode, num}
				(*thisNode).edges = append((*thisNode).edges, edge)
			}
			if rowIdx > 0 {
				thatNode := nodes[(rowIdx-1)*len(input)+colIdx]
				num := int(rune(input[rowIdx-1][colIdx]) - '0')
				edge := Edge{thisNode, thatNode, num}
				(*thisNode).edges = append((*thisNode).edges, edge)
			}
			if rowIdx < len(input)-1 {
				thatNode := nodes[(rowIdx+1)*len(input)+colIdx]
				num := int(rune(input[rowIdx+1][colIdx]) - '0')
				edge := Edge{thisNode, thatNode, num}
				(*thisNode).edges = append((*thisNode).edges, edge)
			}
		}
	}

	return nodes[0]
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	rootNode := buildGraph(lines)
	value := findPath(rootNode)
	fmt.Println(value)
}
