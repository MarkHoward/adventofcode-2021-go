package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func updatePosition(direction string, amount int, currentX int, currentY int) (int, int) {
	switch direction {
	case "forward":
		currentX += amount
	case "down":
		currentY += amount
	case "up":
		currentY -= amount
	default:
		fmt.Println("Invalid direction: " + direction)
	}
	return currentX, currentY
}

func convertToCommand(input string) (string, int) {
	splitInput := strings.Split(input, " ")
	amount, err := strconv.Atoi(splitInput[1])
	if err != nil {
		fmt.Println("Invalid amount: " + input)
	}
	return splitInput[0], amount
}

func getPosition(input []string) (int, int) {
	var currentX, currentY int
	for _, value := range input {
		direction, amount := convertToCommand(value)
		currentX, currentY = updatePosition(direction, amount, currentX, currentY)
	}
	return currentX, currentY
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	finalX, finalY := getPosition(lines)
	fmt.Println(finalX * finalY)
}
