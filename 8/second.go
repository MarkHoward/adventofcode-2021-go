package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func decode(outputs []string, key map[string]string) int {
	var combined string
	for _, output := range outputs {
		for k, v := range key {
			if len(k) == len(output) {
				flag := false
				for _, char := range output {
					if !strings.ContainsRune(k, char) {
						flag = true
					}
				}
				if !flag {
					combined += v
					break
				}
			}
		}
	}

	val, err := strconv.Atoi(combined)
	if err != nil {
		fmt.Println(err)
	}
	return val
}

func buildKey(inputs []string) map[string]string {
	keyMap := make(map[string]string)

	sort.Slice(inputs, func(i, j int) bool {
		return len(inputs[i]) < len(inputs[j])
	})
	one, four := inputs[0], inputs[2]

	onePiece, twoPiece := []rune(one)[0], []rune(one)[1]
	for idx, input := range inputs[6:9] {
		if !strings.ContainsRune(input, onePiece) || !strings.ContainsRune(input, twoPiece) {
			keyMap[inputs[idx+6]] = "6"
		} else {
			flag := false
			for _, char := range four {
				if !strings.ContainsRune(input, char) {
					keyMap[inputs[idx+6]] = "0"
					flag = true
				}
			}
			if !flag {
				keyMap[inputs[idx+6]] = "9"
			}
		}
	}
	for idx, input := range inputs[3:6] {
		if !strings.ContainsRune(input, onePiece) || !strings.ContainsRune(input, twoPiece) {
			flag := false
			for _, char := range four {
				if char != onePiece && char != twoPiece && !strings.ContainsRune(input, char) {
					keyMap[inputs[idx+3]] = "2"
					flag = true
				}
			}
			if !flag {
				keyMap[inputs[idx+3]] = "5"
			}
		} else {
			keyMap[inputs[idx+3]] = "3"
		}
	}
	keyMap[inputs[0]] = "1"
	keyMap[inputs[1]] = "7"
	keyMap[inputs[2]] = "4"
	keyMap[inputs[9]] = "8"
	return keyMap
}

func parseLines(lines []string) ([][]string, [][]string) {
	var inputs, outputs [][]string
	for _, line := range lines {
		inOut := strings.Split(line, " | ")
		in, out := inOut[0], inOut[1]
		inputs = append(inputs, strings.Split(in, " "))
		outputs = append(outputs, strings.Split(out, " "))
	}
	return inputs, outputs
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	var count int
	inputs, outputs := parseLines(lines)
	for i := 0; i < len(inputs); i++ {
		key := buildKey(inputs[i])
		count += decode(outputs[i], key)
	}
	fmt.Println(count)
}
