// Error that I can't find, results aren't consistent on each run
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type GameResults map[Game]int

type Game struct {
	player1     Player
	player2     Player
	currentTurn int
}

type Player struct {
	points       int
	currentSpace int
}

func (r GameResults) AllComplete() bool {
	for game := range r {
		if !game.IsDone() {
			return false
		}
	}
	return true
}

func (g Game) PlayTurn() GameResults {
	outcomes := map[int]int{3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
	results := make(GameResults)

	for movement, count := range outcomes {
		var newGame Game
		newPlayer1 := Player{g.player1.points, g.player1.currentSpace}
		newPlayer2 := Player{g.player2.points, g.player2.currentSpace}
		if g.currentTurn == 1 {
			newPlayer1.TakeTurn(movement)
			newGame.currentTurn = 2
		} else {
			newPlayer2.TakeTurn(movement)
			newGame.currentTurn = 1
		}
		newGame.player1 = newPlayer1
		newGame.player2 = newPlayer2
		results[newGame] = count
	}

	return results
}

func (g Game) IsDone() bool {
	return g.player1.points >= 21 || g.player2.points >= 21
}

func (p *Player) TakeTurn(movement int) {
	newSpace := (p.currentSpace + movement) % 10
	if newSpace == 0 {
		newSpace = 10
	}
	(*p).points += newSpace
	(*p).currentSpace = newSpace
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	starting1, err := strconv.Atoi(strings.Split(lines[0], ": ")[1])
	if err != nil {
		panic(err)
	}
	starting2, err := strconv.Atoi(strings.Split(lines[1], ": ")[1])
	if err != nil {
		panic(err)
	}

	player1 := Player{0, starting1}
	player2 := Player{0, starting2}
	game1 := Game{player1, player2, 1}
	results := make(GameResults)
	results[game1] = 1

	for !results.AllComplete() {
		newResults := make(GameResults)
		for game, count := range results {
			if game.IsDone() {
				newResults[game] = count
				continue
			}
			gameResults := game.PlayTurn()
			for g := range gameResults {
				if _, ok := newResults[g]; !ok {
					newResults[g] = gameResults[g] * count
				} else {
					newResults[g] += gameResults[g] * count
				}
			}
		}
		results = newResults
	}

	var player1Wins, player2Wins int
	for game, count := range results {
		if game.player1.points > game.player2.points {
			player1Wins += count
		} else {
			player2Wins += count
		}
	}
	fmt.Println(player1Wins, player2Wins)
}
