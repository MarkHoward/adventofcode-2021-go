package main

import (
	"bufio"
	"fmt"
	"os"
)

type Octopus struct {
	energy   int
	canFlash bool
}

func flash(grid *[][]Octopus, row int, col int) int {
	var count int
	(*grid)[row][col].energy = 0
	(*grid)[row][col].canFlash = false
	if row > 0 {
		count += gainEnergy(grid, row-1, col)
		if col > 0 {
			count += gainEnergy(grid, row-1, col-1)
		}
		if col < len(*grid)-1 {
			count += gainEnergy(grid, row-1, col+1)
		}
	}
	if row < len(*grid)-1 {
		count += gainEnergy(grid, row+1, col)
		if col > 0 {
			count += gainEnergy(grid, row+1, col-1)
		}
		if col < len(*grid)-1 {
			count += gainEnergy(grid, row+1, col+1)
		}
	}
	if col > 0 {
		count += gainEnergy(grid, row, col-1)
	}
	if col < len(*grid)-1 {
		count += gainEnergy(grid, row, col+1)
	}
	return count
}

func gainEnergy(grid *[][]Octopus, row int, col int) int {
	if (*grid)[row][col].canFlash {
		(*grid)[row][col].energy++
		if (*grid)[row][col].energy > 9 {
			return 1 + flash(grid, row, col)
		}
	}
	return 0
}

func step(grid *[][]Octopus) int {
	var count int

	for row := range *grid {
		for col := range (*grid)[row] {
			count += gainEnergy(grid, row, col)
		}
	}
	for row := range *grid {
		for col := range (*grid)[row] {
			(*grid)[row][col].canFlash = true
		}
	}

	return count
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var grid [][]Octopus
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var line []Octopus
		for _, char := range scanner.Text() {
			line = append(line, Octopus{int(char - '0'), true})
		}
		grid = append(grid, line)
	}
	var count int
	for i := 0; i < 100; i++ {
		count += step(&grid)
	}
	fmt.Println(count)
}
