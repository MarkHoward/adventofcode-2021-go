// FML this is awful

package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

type Number struct {
	parent *Number
	left   interface{}
	right  interface{}
}

func (n *Number) String() string {
	result := "["

	if left, ok := n.left.(*Number); ok {
		result += fmt.Sprint(left)
	}
	if left, ok := n.left.(*int); ok {
		result += fmt.Sprint(*left)
	}

	result += ","

	if right, ok := n.right.(*Number); ok {
		result += fmt.Sprint(right)
	}
	if right, ok := n.right.(*int); ok {
		result += fmt.Sprint(*right)
	}

	result += "]"
	return result
}

func (n *Number) findLeftAddAsc(caller *Number) *int {
	if left, ok := n.left.(*int); ok {
		return left
	}
	if n.right == caller {
		if left, ok := n.left.(*Number); ok {
			return left.findRightAddDesc()
		}
	}
	if n.parent == nil {
		return nil
	}
	return n.parent.findLeftAddAsc(n)
}

func (n *Number) findLeftAddDesc() *int {
	if left, ok := n.left.(*int); ok {
		return left
	}
	if left, ok := n.left.(*Number); ok {
		leftCheck := left.findLeftAddDesc()
		if leftCheck != nil {
			return leftCheck
		}
		if right, ok := n.right.(*int); ok {
			return right
		}
		if right, ok := n.right.(*Number); ok {
			return right.findLeftAddDesc()
		}
	}
	return nil
}

func (n *Number) findRightAddAsc(caller *Number) *int {
	if right, ok := n.right.(*int); ok {
		return right
	}
	if n.left == caller {
		if right, ok := n.right.(*Number); ok {
			return right.findLeftAddDesc()
		}
	}
	if n.parent == nil {
		return nil
	}
	return n.parent.findRightAddAsc(n)
}

func (n *Number) findRightAddDesc() *int {
	if right, ok := n.right.(*int); ok {
		return right
	}
	if right, ok := n.right.(*Number); ok {
		rightCheck := right.findRightAddDesc()
		if rightCheck != nil {
			return rightCheck
		}
		if left, ok := n.left.(*int); ok {
			return left
		}
		if left, ok := n.left.(*Number); ok {
			return left.findRightAddDesc()
		}
	}
	return nil
}

func (n *Number) explode() {
	if left, ok := n.left.(*int); ok {
		leftChange := n.parent.findLeftAddAsc(n)
		if leftChange != nil {
			*leftChange += *left
		}
	} else {
		panic("too deep")
	}
	if right, ok := n.right.(*int); ok {
		rightChange := n.parent.findRightAddAsc(n)
		if rightChange != nil {
			*rightChange += *right
		}
	} else {
		panic("too deep")
	}
	newVal := 0
	if n.parent.left == n {
		(*n).parent.left = &newVal
	}
	if n.parent.right == n {
		(*n).parent.right = &newVal
	}
}

func (n *Number) findExplosions(depth int) bool {
	if depth > 4 {
		n.explode()
		return true
	}
	if left, ok := n.left.(*Number); ok {
		if left.findExplosions(depth + 1) {
			return true
		}
	}
	if right, ok := n.right.(*Number); ok {
		if right.findExplosions(depth + 1) {
			return true
		}
	}
	return false
}

func (n *Number) findSplit() bool {
	if left, ok := n.left.(*int); ok {
		if *left >= 10 {
			leftVal := *left / 2
			rightVal := int(math.Ceil(float64(*left) / 2.0))
			(*n).left = &Number{n, &leftVal, &rightVal}
			return true
		}
	}
	if left, ok := n.left.(*Number); ok {
		if left.findSplit() {
			return true
		}
	}
	if right, ok := n.right.(*int); ok {
		if *right >= 10 {
			leftVal := *right / 2
			rightVal := int(math.Ceil(float64(*right) / 2.0))
			(*n).right = &Number{n, &leftVal, &rightVal}
			return true
		}
	}
	if right, ok := n.right.(*Number); ok {
		if right.findSplit() {
			return true
		}
	}

	return false
}

func (n *Number) reduce() {
	for n.findExplosions(1) {
	}
	if n.findSplit() {
		n.reduce()
	}
}

func (n *Number) getMagnitude() int {
	var leftVal, rightVal int
	if left, ok := n.left.(*int); ok {
		leftVal = *left
	}
	if right, ok := n.right.(*int); ok {
		rightVal = *right
	}
	if left, ok := n.left.(*Number); ok {
		leftVal = left.getMagnitude()
	}
	if right, ok := n.right.(*Number); ok {
		rightVal = right.getMagnitude()
	}
	return 3*leftVal + 2*rightVal
}

func add(x *Number, y *Number) *Number {
	newNumber := &Number{nil, x, y}
	x.parent = newNumber
	y.parent = newNumber
	newNumber.reduce()
	return newNumber
}

func parseNumber(line string) *Number {
	var nums []*Number

	for _, char := range line {
		switch char {
		case '[':
			nums = append(nums, &Number{})
		case ']':
			if len(nums) > 1 {
				if nums[len(nums)-2].left == nil {
					nums[len(nums)-2].left = nums[len(nums)-1]
				} else {
					nums[len(nums)-2].right = nums[len(nums)-1]
				}
				nums[len(nums)-1].parent = nums[len(nums)-2]
				nums = nums[:len(nums)-1]
			}
		case ',':
			continue
		default:
			val := int(char - '0')
			if nums[len(nums)-1].left == nil {
				nums[len(nums)-1].left = &val
			} else {
				nums[len(nums)-1].right = &val
			}
		}
	}
	return nums[0]
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	result := parseNumber(lines[0])
	for _, line := range lines[1:] {
		num := parseNumber(line)
		result = add(result, num)
	}
	fmt.Println(result)
	fmt.Println(result.getMagnitude())
}
