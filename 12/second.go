package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Cave struct {
	name        string
	connections []*Cave
}

func (c Cave) IsSmall() bool {
	return strings.ToLower(c.name) == c.name
}

func findCave(array []*Cave, name string) int {
	for idx, c := range array {
		if c.name == name {
			return idx
		}
	}
	return -1
}

func pathContainsCave(path []string, name string) bool {
	for _, s := range path {
		if s == name {
			return true
		}
	}
	return false
}

func makeCaves(lines []string) []*Cave {
	var caves []*Cave

	for _, line := range lines {
		entries := strings.Split(line, "-")
		cave1Idx := findCave(caves, entries[0])
		cave2Idx := findCave(caves, entries[1])

		if cave1Idx == -1 {
			caves = append(caves, &Cave{entries[0], make([]*Cave, 0)})
			cave1Idx = findCave(caves, entries[0])
		}
		if cave2Idx == -1 {
			caves = append(caves, &Cave{entries[1], make([]*Cave, 0)})
			cave2Idx = findCave(caves, entries[1])
		}
		caves[cave1Idx].connections = append(caves[cave1Idx].connections, caves[cave2Idx])
		caves[cave2Idx].connections = append(caves[cave2Idx].connections, caves[cave1Idx])
	}

	return caves
}

func findPaths(cave Cave, currentPath []string, containsDouble bool) [][]string {
	var paths [][]string
	if cave.name == "end" {
		paths = append(paths, append(currentPath, "end"))
	} else {
		for _, conn := range cave.connections {
			willHaveDouble := containsDouble
			if conn.name == "start" {
				continue
			}
			if conn.IsSmall() && pathContainsCave(currentPath, conn.name) {
				if containsDouble {
					continue
				}
				willHaveDouble = true
			}
			possiblePaths := findPaths(*conn, append(currentPath, cave.name), willHaveDouble)
			for _, path := range possiblePaths {
				paths = append(paths, path)
			}
		}
	}

	return paths
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Error opening input.txt")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	caves := makeCaves(lines)
	startCaveIdx := findCave(caves, "start")
	paths := findPaths(*caves[startCaveIdx], make([]string, 0), false)
	fmt.Println(len(paths))
}
